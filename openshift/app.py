from flask import Flask, render_template, request, make_response
import os
import socket
import random
import json
import requests
import urllib3

hostname = socket.gethostname()

app = Flask(__name__)

@app.route("/", methods=['POST'])
def entry_point():
   params = request.get_json()
   list1 = [params.get("Gender"),
            params.get("Status"),
            params.get("Children"),
	    params.get("Income"),
	    params.get("Car"),
	    params.get("Age"),
	    params.get("LongDist"),
	    params.get("International"),
	    params.get("local"),
	    params.get("dropped"),
	    params.get("paymethod"),
	    params.get("localbilltype"),
	    params.get("longbilltype"),
	    params.get("usage"),
	    params.get("rate-plan")
           ]

   #list1 = ["M", "M", 0, 19000, "N", 50, 24, 0, 22, 0, "CC", "FreeLocal", "Standard", 47, 3]

   iam_token='eyJraWQiOiIyMDIwMDMyNjE4MjgiLCJhbGciOiJSUzI1NiJ9.eyJpYW1faWQiOiJpYW0tU2VydmljZUlkLWQzNmE0NmUwLTI4MjktNDE3Yi1hZmUwLTNiMWVhZDBkZjBhYiIsImlkIjoiaWFtLVNlcnZpY2VJZC1kMzZhNDZlMC0yODI5LTQxN2ItYWZlMC0zYjFlYWQwZGYwYWIiLCJyZWFsbWlkIjoiaWFtIiwiaWRlbnRpZmllciI6IlNlcnZpY2VJZC1kMzZhNDZlMC0yODI5LTQxN2ItYWZlMC0zYjFlYWQwZGYwYWIiLCJuYW1lIjoiU2VydmljZSBjcmVkZW50aWFscy0xIiwic3ViIjoiU2VydmljZUlkLWQzNmE0NmUwLTI4MjktNDE3Yi1hZmUwLTNiMWVhZDBkZjBhYiIsInN1Yl90eXBlIjoiU2VydmljZUlkIiwiYWNjb3VudCI6eyJ2YWxpZCI6dHJ1ZSwiYnNzIjoiMzM3OTM0NzBiYjg3NjkyMWMyMzFmYzMwOTFjYTAwYmUifSwiaWF0IjoxNTg2Nzk2MTAxLCJleHAiOjE1ODY3OTk3MDEsImlzcyI6Imh0dHBzOi8vaWFtLm5nLmJsdWVtaXgubmV0L29pZGMvdG9rZW4iLCJncmFudF90eXBlIjoidXJuOmlibTpwYXJhbXM6b2F1dGg6Z3JhbnQtdHlwZTphcGlrZXkiLCJzY29wZSI6ImlibSBvcGVuaWQiLCJjbGllbnRfaWQiOiJieCIsImFjciI6MSwiYW1yIjpbInB3ZCJdfQ.WW9viY9csEog2WEH2nEehUul-kuQ0WFMh2Jap8sTatLQ8S2oDlAvmGS2BWO7nZCFXKZcOGBO5EWRIY0GqZFiKjcPF-zWJId88I2Gsk6VT6X0U6hsoHTeeY9LN8C7Ozm96ZH56eG9GfAH6HNo5Rrnxo2EluDTlyIZyMxE3EhIBeDgCulsP86ZwVCNQyo5S9lLPRFUINCL6b9KLdAwhpzOo9RZEZivymb0KrxQN2ETQ_G82czrRvPuJWhh45XX7ho1skZAFpRmK-Gf-cIu39k6z-dA5IC1sbJuoU5SFg_Pwriwjh7w_HdfqwITiFdWc1XWFUDG1OJ47LSudM8gt1dBIg'

   ml_instance_id = 'ef930083-c225-4cd1-ab7d-d9f7148f64b7'

   # NOTE: generate iam_token and retrieve ml_instance_id based on provided documentation
   header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + iam_token, 'ML-Instance-ID': ml_instance_id}

   # NOTE: manually define and pass the array(s) of values to be scored in the next line
   payload_scoring = {"input_data": [{"fields": ["Gender", "Status", "Children", "Est Income", "Car Owner", "Age", "LongDistance", "International", "Local", "Dropped", "Paymethod", "LocalBilltype", "LongDistanceBilltype", "Usage", "RatePlan"], "values": [list1, ["F","S",2,29000,"N",41,27,0,58,0,"Auto","Budget","Standard",86,2]]}]}
   #payload_scoring = {"input_data": [{"fields": ["Gender", "Status", "Children", "Est Income", "Car Owner", "Age", "LongDistance", "International", "Local", "Dropped", "Paymethod", "LocalBilltype", "LongDistanceBilltype", "Usage", "RatePlan"], "values": [array_of_values_to_be_scored, another_array_of_values_to_be_scored]}]}

   response_scoring = requests.post('https://us-south.ml.cloud.ibm.com/v4/deployments/9a0db85e-aca6-47ba-a6c0-f1ad705252a3/predictions', json=payload_scoring, headers=header)
   print("Scoring response")
   print(json.loads(response_scoring.text))
   json_response = response_scoring.json()
   return json_response

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True, threaded=True)
