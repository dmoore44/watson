import sys
import json
import requests
import urllib3

'''
payload looks like this
{
	"Gender": "M",
	"Status": "M",
	"Children": 0,
	"Income": 19000,
	"Car": "N",
	"Age": 50,
	"LongDist": 24,
	"International": 0,
	"local": 22,
	"dropped": 0,
	"paymethod": "CC",
	"localbilltype": "FreeLocal",
	"longbilltype": "Standard",
	"usage": 47,
	"rate-plan": 3
}

The data that is sent to Watson needs to look like this:
{
	"input_data": [{
		"fields": ["Gender", "Status", "Children", "Est Income", "Car Owner", "Age", "LongDistance", "International", "Local", "Dropped", "Paymethod", "LocalBilltype", "LongDistanceBilltype", "Usage", "RatePlan"],
		"values": [
			["M", "M", 0, 19000, "N", 50, 24, 0, 22, 0, "CC", "FreeLocal", "Standard", 47, 3],
			["F", "S", 2, 29000, "N", 41, 27, 0, 58, 0, "Auto", "Budget", "Standard", 86, 2]
		]
	}]
}


'''

#
# main() will be run when you invoke this action
# @param Cloud Functions actions accept a single parameter, which must be a JSON object.
# @return The output of this action, which must be a JSON object.
#
def main(params):
    return call_watson(params)

def call_watson(params):
   list1 = [params.get("Gender"),
            params.get("Status"),
	        params.get("Children"),
	        params.get("Income"),
	        params.get("Car"),
	        params.get("Age"),
	        params.get("LongDist"),
	        params.get("International"),
	        params.get("local"),
	        params.get("dropped"),
	        params.get("paymethod"),
	        params.get("localbilltype"),
	        params.get("longbilltype"),
	        params.get("usage"),
	        params.get("rate-plan")
           ]

   #list1 = 	["M", "M", 0, 19000, "N", 50, 24, 0, 22, 0, "CC", "FreeLocal", "Standard", 47, 3]

   iam_token = 'eyJraWQiOiIyMDIwMDMyNjE4MjgiLCJhbGciOiJSUzI1NiJ9.eyJpYW1faWQiOiJpYW0tU2VydmljZUlkLWQzNmE0NmUwLTI4MjktNDE3Yi1hZmUwLTNiMWVhZDBkZjBhYiIsImlkIjoiaWFtLVNlcnZpY2VJZC1kMzZhNDZlMC0yODI5LTQxN2ItYWZlMC0zYjFlYWQwZGYwYWIiLCJyZWFsbWlkIjoiaWFtIiwiaWRlbnRpZmllciI6IlNlcnZpY2VJZC1kMzZhNDZlMC0yODI5LTQxN2ItYWZlMC0zYjFlYWQwZGYwYWIiLCJuYW1lIjoiU2VydmljZSBjcmVkZW50aWFscy0xIiwic3ViIjoiU2VydmljZUlkLWQzNmE0NmUwLTI4MjktNDE3Yi1hZmUwLTNiMWVhZDBkZjBhYiIsInN1Yl90eXBlIjoiU2VydmljZUlkIiwiYWNjb3VudCI6eyJ2YWxpZCI6dHJ1ZSwiYnNzIjoiMzM3OTM0NzBiYjg3NjkyMWMyMzFmYzMwOTFjYTAwYmUifSwiaWF0IjoxNTg2Nzg0MTMwLCJleHAiOjE1ODY3ODc3MzAsImlzcyI6Imh0dHBzOi8vaWFtLm5nLmJsdWVtaXgubmV0L29pZGMvdG9rZW4iLCJncmFudF90eXBlIjoidXJuOmlibTpwYXJhbXM6b2F1dGg6Z3JhbnQtdHlwZTphcGlrZXkiLCJzY29wZSI6ImlibSBvcGVuaWQiLCJjbGllbnRfaWQiOiJieCIsImFjciI6MSwiYW1yIjpbInB3ZCJdfQ.SCJLAI37g7ymmEoTcf4KDa0d_AuugZOiX8xP1giuy6obJt91sEI9wk4fCoK5E5F6IZdZ_teG0K98FpzifYfQ1HZivHIe3MmQHaTxHAhiR6H9Ung2ZRYleb-2rmxAqjgoII4Dm5bs1uyM4qHQJ8SFtdS8Lfks-2SER2VlDXGw6tletZ82bgAlL5itAPCf_iVfnTBanY5ds84Y-CfREvAZzWFVxzRUBbs29DOojlf7f7lQtaLhYFEGiD9uGEhmmkQn5-83zFQ0J5lD3WuMBh49aHjokBfzX1yBO1LtkyF6OA-bEioaJcM7JS7jN0uupnlBEkBhNv1uZOdV5X1BlwJobg'

   ml_instance_id = 'ef930083-c225-4cd1-ab7d-d9f7148f64b7'

   # NOTE: generate iam_token and retrieve ml_instance_id based on provided documentation
   header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + iam_token, 'ML-Instance-ID': ml_instance_id}

   # NOTE: manually define and pass the array(s) of values to be scored in the next line
   payload_scoring = {"input_data": [{"fields": ["Gender", "Status", "Children", "Est Income", "Car Owner", "Age", "LongDistance", "International", "Local", "Dropped", "Paymethod", "LocalBilltype", "LongDistanceBilltype", "Usage", "RatePlan"], "values": [list1, ["F","S",2,29000,"N",41,27,0,58,0,"Auto","Budget","Standard",86,2]]}]}
   #payload_scoring = {"input_data": [{"fields": ["Gender", "Status", "Children", "Est Income", "Car Owner", "Age", "LongDistance", "International", "Local", "Dropped", "Paymethod", "LocalBilltype", "LongDistanceBilltype", "Usage", "RatePlan"], "values": [array_of_values_to_be_scored, another_array_of_values_to_be_scored]}]}

   response_scoring = requests.post('https://us-south.ml.cloud.ibm.com/v4/deployments/9a0db85e-aca6-47ba-a6c0-f1ad705252a3/predictions', json=payload_scoring, headers=header)
   print("Scoring response")
   print(json.loads(response_scoring.text))

   json_response = response_scoring.json()

   return json_response
